/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author informatics
 */
public class Player {
    private char symbol;
    private int winCount,loseCount,drawCount;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
    
    public void win() {
        winCount += 1;
    }
    
    public void lose() {
        loseCount += 1;
    }
    
    public void draw() {
        drawCount += 1;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", DrawCount=" + drawCount + '}';
    }
    
}
